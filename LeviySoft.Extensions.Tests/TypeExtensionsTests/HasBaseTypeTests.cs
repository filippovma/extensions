﻿using Xunit;

namespace LeviySoft.Extensions.Tests.TypeExtensionsTests
{
    class Base { }
    class Clazz : Base { }
    class Last : Clazz { }
    class NotInHierarchy { }

    public class HasBaseTypeTests
    {
        [Fact]
        public void HasBaseTypeReturnsFalseOnTypeItself()
        {
            Assert.False(typeof(Base).HasBaseType(typeof(Base)));
        }

        [Fact]
        public void HasBaseTypeReturnsTrueOnSingleLevelInheritance()
        {
            Assert.True(typeof(Clazz).HasBaseType(typeof(Base)));
            Assert.True(typeof(Last).HasBaseType(typeof(Clazz)));
        }

        [Fact]
        public void HasBaseTypeReturnsTrueOnMultipleLevelInheritance()
        {
            Assert.True(typeof(Last).HasBaseType(typeof(Base)));
        }

        [Fact]
        public void HasBaseTypeReturnsFalseOnReverseInheritance()
        {
            Assert.False(typeof(Base).HasBaseType(typeof(Clazz)));
            Assert.False(typeof(Clazz).HasBaseType(typeof(Last)));
            Assert.False(typeof(Base).HasBaseType(typeof(Last)));
        }

        [Fact]
        public void HasBaseTypeReturnsFalseWhenClassesAreNotAPartOfHierarchy()
        {
            Assert.False(typeof(NotInHierarchy).HasBaseType(typeof(Base)));
            Assert.False(typeof(NotInHierarchy).HasBaseType(typeof(Clazz)));
            Assert.False(typeof(NotInHierarchy).HasBaseType(typeof(Last)));
            Assert.False(typeof(Base).HasBaseType(typeof(NotInHierarchy)));
            Assert.False(typeof(Clazz).HasBaseType(typeof(NotInHierarchy)));
            Assert.False(typeof(Last).HasBaseType(typeof(NotInHierarchy)));
        }
    }
}
