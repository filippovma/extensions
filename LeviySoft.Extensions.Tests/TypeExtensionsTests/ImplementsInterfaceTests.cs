﻿using Xunit;

namespace LeviySoft.Extensions.Tests.TypeExtensionsTests
{
    interface Dummy { }
    interface ITest { }
    interface ITest<T> { }
    class Test : ITest { }
    class GenericTest<T> : ITest<T> { }
    class ExtendedGenericTest<T> : ITest<T>, ITest {}
    class NestedTest : Test { }
    class NestedGenericTest<T> : GenericTest<T> { }
    class NestedExtendedGenericTest<T> : ExtendedGenericTest<T> { }

    public class ImplementsInterfaceTests
    {
        [Fact]
        public void ImplementsInterfaceReturnsFalseForANotInHierarchyInterface()
        {
            Assert.False(typeof(Test).ImplementsInterface(typeof(Dummy)));
            Assert.False(typeof(GenericTest<>).ImplementsInterface(typeof(Dummy)));
            Assert.False(typeof(ExtendedGenericTest<>).ImplementsInterface(typeof(Dummy)));
            Assert.False(typeof(NestedTest).ImplementsInterface(typeof(Dummy)));
            Assert.False(typeof(NestedGenericTest<>).ImplementsInterface(typeof(Dummy)));
            Assert.False(typeof(NestedExtendedGenericTest<>).ImplementsInterface(typeof(Dummy)));
        }

        [Fact]
        public void ImplementsInterfaceReturnsTrueOnSinglePlainInterface()
        {
            Assert.True(typeof(Test).ImplementsInterface(typeof(ITest)));
        }

        [Fact]
        public void ImplementsInterfaceReturnsTrueOnSingleGenericInterface()
        {
            Assert.True(typeof(GenericTest<>).ImplementsInterface(typeof(ITest<>)));
        }

        [Fact]
        public void ImplementsInterfaceReturnsTrueOnPlainInterfaceInMultiinterfacedClass()
        {
            Assert.True(typeof(ExtendedGenericTest<>).ImplementsInterface(typeof(ITest)));
        }

        [Fact]
        public void ImplementsInterfacerReturnsTrueOnGenericInterfaceInMultiinterfacedClass()
        {
            Assert.True(typeof(ExtendedGenericTest<>).ImplementsInterface(typeof(ITest<>)));
        }

        [Fact]
        public void ImplementsInterfaceReturnsTrueOnSinglePlainInterfaceInNestedClass()
        {
            Assert.True(typeof(NestedTest).ImplementsInterface(typeof(ITest)));
        }

        [Fact]
        public void ImplementsInterfaceReturnsTrueOnSingleGenericInterfaceInNestedClass()
        {
            Assert.True(typeof(NestedGenericTest<>).ImplementsInterface(typeof(ITest<>)));
        }

        [Fact]
        public void ImplementsInterfaceReturnsTrueOnPlainInterfaceInNestedMultiinterfacedClass()
        {
            Assert.True(typeof(NestedExtendedGenericTest<>).ImplementsInterface(typeof(ITest)));
        }

        [Fact]
        public void ImplementsInterfaceReturnsTrueOnGenericInterfaceInNestedMultiinterfacedClass()
        {
            Assert.True(typeof(NestedExtendedGenericTest<>).ImplementsInterface(typeof(ITest<>)));
        }
    }
}
