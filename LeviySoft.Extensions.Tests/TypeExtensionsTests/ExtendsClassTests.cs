﻿using Xunit;

namespace LeviySoft.Extensions.Tests.TypeExtensionsTests
{
    class Container<T> { }
    class ClazzContainer<T> : Container<T> { }
    class ExtendedClazzContainer<T> : ClazzContainer<T> { }

    public class ExtendsClassTests
    {
        [Fact]
        public void ExtendsClassReturnsFalseOnTypeItself()
        {
            Assert.False(typeof(Base).ExtendsClass(typeof(Base)));
            Assert.False(typeof(Container<>).ExtendsClass(typeof(Container<>)));
        }

        [Fact]
        public void ExtendsClassReturnsTrueOnSingleLevelInheritance()
        {
            Assert.True(typeof(Clazz).ExtendsClass(typeof(Base)));
            Assert.True(typeof(ClazzContainer<>).ExtendsClass(typeof(Container<>)));
            Assert.True(typeof(Last).ExtendsClass(typeof(Clazz)));
            Assert.True(typeof(ExtendedClazzContainer<>).ExtendsClass(typeof(ClazzContainer<>)));
        }

        [Fact]
        public void ExtendsClassTypeReturnsTrueOnMultipleLevelInheritance()
        {
            Assert.True(typeof(Last).ExtendsClass(typeof(Base)));
            Assert.True(typeof(ExtendedClazzContainer<>).ExtendsClass(typeof(Container<>)));
        }

        [Fact]
        public void ExtendsClassReturnsFalseOnReverseInheritance()
        {
            Assert.False(typeof(Base).ExtendsClass(typeof(Clazz)));
            Assert.False(typeof(Clazz).ExtendsClass(typeof(Last)));
            Assert.False(typeof(Base).ExtendsClass(typeof(Last)));
        }

        [Fact]
        public void ExtendsClassReturnsFalseWhenClassesAreNotAPartOfHierarchy()
        {
            Assert.False(typeof(NotInHierarchy).ExtendsClass(typeof(Base)));
            Assert.False(typeof(NotInHierarchy).ExtendsClass(typeof(Clazz)));
            Assert.False(typeof(NotInHierarchy).ExtendsClass(typeof(Last)));
            Assert.False(typeof(Base).ExtendsClass(typeof(NotInHierarchy)));
            Assert.False(typeof(Clazz).ExtendsClass(typeof(NotInHierarchy)));
            Assert.False(typeof(Last).ExtendsClass(typeof(NotInHierarchy)));
        }
    }
}
