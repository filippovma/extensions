﻿using System.Linq;
using System.Xml.Linq;

namespace LeviySoft.Extensions
{
    /// <summary>
    /// Extension methods for XElement
    /// </summary>
    public static class XElementExtensions
    {
        /// <summary>
        /// Checks attribute presence
        /// </summary>
        /// <param name="element">XElement instance</param>
        /// <param name="attributeName">Attribute name</param>
        /// <returns>Yes/no</returns>
        public static bool HasAttribute(this XElement element, string attributeName)
        {
            return element.Attributes().Any(a => a.Name.LocalName == attributeName);
        }

        /// <summary>
        /// Checks presence of a set of attributes
        /// </summary>
        /// <param name="element">XElement instance</param>
        /// <param name="attributeNames">Attribute name</param>
        /// <returns>Yes/no</returns>
        public static bool HasAttributes(this XElement element, params string[] attributeNames)
        {
            return attributeNames.All(element.HasAttribute);
        }
    }
}
