﻿using System;
using System.Net;

namespace LeviySoft.Extensions
{
    /// <summary>
    /// Extension methods for IPAddress
    /// </summary>
    public static class IPAddressExtensions
    {
        /// <summary>
        /// Computes subnetwok's broadcast address
        /// </summary>
        /// <param name="address">IP address from subnet</param>
        /// <param name="subnetMask">Subnet mask</param>
        /// <returns>Broadcast IP</returns>
        public static IPAddress GetBroadcastAddress(this IPAddress address, IPAddress subnetMask)
        {
            byte[] ipAdressBytes = address.GetAddressBytes();
            byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

            if (ipAdressBytes.Length != subnetMaskBytes.Length)
                throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

            byte[] broadcastAddress = new byte[ipAdressBytes.Length];
            for (int i = 0; i < broadcastAddress.Length; i++)
            {
                broadcastAddress[i] = (byte)(ipAdressBytes[i] | (subnetMaskBytes[i] ^ 255));
            }
            return new IPAddress(broadcastAddress);
        }

        /// <summary>
        /// Computes subnetwork's network address
        /// </summary>
        /// <param name="address">IP address from subnet</param>
        /// <param name="subnetMask">Subnet mask</param>
        /// <returns>Network address</returns>
        public static IPAddress GetNetworkAddress(this IPAddress address, IPAddress subnetMask)
        {
            byte[] ipAdressBytes = address.GetAddressBytes();
            byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

            if (ipAdressBytes.Length != subnetMaskBytes.Length)
                throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

            byte[] broadcastAddress = new byte[ipAdressBytes.Length];
            for (int i = 0; i < broadcastAddress.Length; i++)
            {
                broadcastAddress[i] = (byte)(ipAdressBytes[i] & (subnetMaskBytes[i]));
            }
            return new IPAddress(broadcastAddress);
        }

        /// <summary>
        /// Check if two IP addresses are from the same subnet
        /// </summary>
        /// <param name="address2">IP 1</param>
        /// <param name="address">IP 2</param>
        /// <param name="subnetMask">Subnet mask</param>
        public static bool IsInSameSubnet(this IPAddress address2, IPAddress address, IPAddress subnetMask)
        {
            IPAddress network1 = address.GetNetworkAddress(subnetMask);
            IPAddress network2 = address2.GetNetworkAddress(subnetMask);

            return network1.Equals(network2);
        }

        /// <summary>
        /// Converts IPAddress instance to int representing netmask length
        /// e.g. 255.255.255.0 => 24
        /// </summary>
        /// <param name="netmask"></param>
        /// <returns></returns>
        public static int ToNetmaskBitLength(this IPAddress netmask)
        {
            int totalBits = 0;
            foreach (string octet in netmask.ToString().Split('.'))
            {
                byte octetByte = byte.Parse(octet);
                while (octetByte != 0)
                {
                    totalBits += octetByte & 1;     // logical AND on the LSB
                    octetByte >>= 1;            // do a bitwise shift to the right to create a new LSB
                }
            }
            return totalBits;
        }
    }
}
