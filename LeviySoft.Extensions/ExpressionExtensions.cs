﻿using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace LeviySoft.Extensions
{
    /// <summary>
    /// Extansions for Expressions
    /// </summary>
    public static class ExpressionExtensions
    {
        /// <summary>
        /// Extract value with type T from expression.
        /// </summary>
        /// <param name="expression">Expression</param>
        /// <returns></returns>
        public static object ExtractValue(this Expression expression)
        {
            var constantExpression = expression as ConstantExpression;
            if (constantExpression != null) return constantExpression.Value;
            var unaryExpression = expression as UnaryExpression;
            if (unaryExpression != null) return ExtractValue(unaryExpression.Operand);
            var methodCallExpression = expression as MethodCallExpression;
            if (methodCallExpression != null) return methodCallExpression.Method.Invoke(ExtractValue(methodCallExpression.Object), methodCallExpression.Arguments.Select(ExtractValue).ToArray());
            var memberExpression = expression as MemberExpression;
            if (memberExpression == null) return null;
            var fieldInfo = memberExpression.Member as FieldInfo;
            if (fieldInfo != null) return fieldInfo.GetValue(ExtractValue(memberExpression.Expression));
            var propertyInfo = memberExpression.Member as PropertyInfo;
            return propertyInfo != null ? propertyInfo.GetValue(ExtractValue(memberExpression.Expression)) : null;
        }
    }
}
