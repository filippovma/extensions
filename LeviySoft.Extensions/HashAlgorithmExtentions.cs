using System.Security.Cryptography;
using System.Text;

namespace LeviySoft.Extensions
{
    /// <summary>
    /// Extension methods for HashAlgorithm
    /// </summary>
    public static class HashAlgorithmExtentions
    {
        /// <summary>
        /// Returns hash string using specified HashAlgorithm instance
        /// </summary>
        /// <param name="hashAlgorithm">HashAlgorithm instance</param>
        /// <param name="input">String to hash</param>
        /// <returns>Hash string</returns>
        public static string HashString(this HashAlgorithm hashAlgorithm, string input)
        {
            byte[] data = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(input));
            var sBuilder = new StringBuilder();
            foreach (byte t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }
            return sBuilder.ToString();
        }
    }
}
