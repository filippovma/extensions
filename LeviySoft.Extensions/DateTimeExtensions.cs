﻿using System;

namespace LeviySoft.Extensions
{
    /// <summary>
    /// Extension methods for DateTime
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Returns first day of week for given DateTime instance
        /// </summary>
        /// <param name="value">DateTime instance</param>
        /// <returns>Result as DateTime instance</returns>
        public static DateTime FirstDayOfWeek(this DateTime value)
        {
            switch (value.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return value;
                case DayOfWeek.Tuesday:
                    return value.AddDays(-1);
                case DayOfWeek.Wednesday:
                    return value.AddDays(-2);
                case DayOfWeek.Thursday:
                    return value.AddDays(-3);
                case DayOfWeek.Friday:
                    return value.AddDays(-4);
                case DayOfWeek.Saturday:
                    return value.AddDays(-5);
                case DayOfWeek.Sunday:
                    return value.AddDays(-6);
                default:
                    return value;
            }
        }

        /// <summary>
        /// Returns first day of month for given DateTime instance
        /// </summary>
        /// <param name="value">DateTime instance</param>
        /// <returns>Result as DateTime instance</returns>
        public static DateTime FirstDayOfMonth(this DateTime value)
        {
            return value.AddDays(-(value.Day - 1));
        }

        /// <summary>
        /// Returns last day of month for given DateTime instance
        /// </summary>
        /// <param name="value">DateTime instance</param>
        /// <returns>Result as DateTime instance</returns>
        public static DateTime LastDayOfMonth(this DateTime value)
        {
            return value.AddMonths(1).AddDays(-value.Day);
        }
    }
}
