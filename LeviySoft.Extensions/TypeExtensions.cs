﻿using System;
using System.Linq;

namespace LeviySoft.Extensions
{
    /// <summary>
    /// Extension methods for Type
    /// </summary>
    public static class TypeExtensions
    {
        /// <summary>
        /// Checks type for specific class nesting
        /// This method can't find unspecified generic types (like List<!--<>-->)
        /// </summary>
        /// <param name="type">Type</param>
        /// <param name="baseType">Target type</param>
        /// <returns>Yes/no</returns>
        public static bool HasBaseType(this Type type, Type baseType)
        {
            if (type.BaseType != null && type.BaseType == baseType)
                return true;
            if (type.BaseType != null)
            {
                return HasBaseType(type.BaseType, baseType);
            }
            return false;
        }

        /// <summary>
        /// Checks Type for specific interface implementation
        /// </summary>
        /// <param name="type">Type</param>
        /// <param name="iface">Desired interface</param>
        /// <returns>Yes/no</returns>
        public static bool ImplementsInterface(this Type type, Type iface)
        {
            Func<Type, bool> implements =
                ti => (ti == iface) || (ti.IsGenericType && ti.GetGenericTypeDefinition() == iface);

            if (implements(type)) return true;
            return type.GetInterfaces().Any(implements);
        }

        /// <summary>
        /// Checks type for specific class nesting
        /// </summary>
        /// <param name="type">Type</param>
        /// <param name="supertype">Target type</param>
        /// <returns>Yes/no</returns>
        public static bool ExtendsClass(this Type type, Type supertype)
        {
            if (supertype.IsGenericTypeDefinition)
            {
                if (type.BaseType != null && type.BaseType.IsGenericType && type.BaseType.GetGenericTypeDefinition() == supertype)
                {
                    return true;
                }
                else if (type.BaseType != null)
                {
                    return ExtendsClass(type.BaseType, supertype);
                }
                return false;
            }
            else
            {
                return type.HasBaseType(supertype);
            }
        }
    }
}
