﻿using System;
using System.Net;

namespace LeviySoft.Extensions
{
    /// <summary>
    /// Subnetworking utility methods
    /// </summary>
    public static class SubnetMask
    {
        /// <summary>
        /// Computes subnet mask using host length
        /// </summary>
        /// <param name="hostpartLength">Host length</param>
        /// <returns>Subnetwork mask</returns>
        public static IPAddress CreateByHostBitLength(int hostpartLength)
        {
            int hostPartLength = hostpartLength;
            int netPartLength = 32 - hostPartLength;

            if (netPartLength < 2)
                throw new ArgumentException("Number of hosts is to large for IPv4");

            Byte[] binaryMask = new byte[4];

            for (int i = 0; i < 4; i++)
            {
                if (i * 8 + 8 <= netPartLength)
                    binaryMask[i] = (byte)255;
                else if (i * 8 > netPartLength)
                    binaryMask[i] = (byte)0;
                else
                {
                    int oneLength = netPartLength - i * 8;
                    string binaryDigit =
                        String.Empty.PadLeft(oneLength, '1').PadRight(8, '0');
                    binaryMask[i] = Convert.ToByte(binaryDigit, 2);
                }
            }
            return new IPAddress(binaryMask);
        }

        /// <summary>
        /// Computes subnet mask by bit length
        /// </summary>
        /// <param name="netpartLength">Bit length</param>
        /// <returns>Subnetwork mask</returns>
        public static IPAddress CreateByNetBitLength(int netpartLength)
        {
            int hostPartLength = 32 - netpartLength;
            return CreateByHostBitLength(hostPartLength);
        }

        /// <summary>
        /// Computes subnet mask by number of hosts
        /// </summary>
        /// <param name="numberOfHosts">Number of hosts</param>
        /// <returns>Subnetwork mask</returns>
        public static IPAddress CreateByHostNumber(int numberOfHosts)
        {
            int maxNumber = numberOfHosts + 1;

            string b = Convert.ToString(maxNumber, 2);

            return CreateByHostBitLength(b.Length);
        }
    }
}
